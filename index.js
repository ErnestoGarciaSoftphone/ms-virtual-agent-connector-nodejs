// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

const path = require('path');

const dotenv = require('dotenv');
// Import required bot configuration.
const ENV_FILE = path.join(__dirname, '.env');
dotenv.config({ path: ENV_FILE });

const restify = require('restify');

// Import required bot services.
// See https://aka.ms/bot-services to learn more about the different parts of a bot.
const { BotFrameworkAdapter } = require('botbuilder');

// This bot's main dialog.
const { EchoBot } = require('./bot');

// Create HTTP server
const server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, () => {
    console.log(`\n${ server.name } listening to ${ server.url }`);
    console.log('\nGet Bot Framework Emulator: https://aka.ms/botframework-emulator');
    console.log('\nTo talk to your bot, open the emulator select "Open Bot"');
});

// Create adapter.
// See https://aka.ms/about-bot-adapter to learn more about how bots work.
const adapter = new BotFrameworkAdapter({
    appId: process.env.MicrosoftAppId,
    appPassword: process.env.MicrosoftAppPassword,
    channelService: process.env.ChannelService,
    openIdMetadata: process.env.BotOpenIdMetadata
});

// Catch-all for errors.
const onTurnErrorHandler = async (context, error) => {
    // This check writes out errors to console log .vs. app insights.
    // NOTE: In production environment, you should consider logging this to Azure
    //       application insights.
    console.error(`\n [onTurnError] unhandled error: ${ error }`);

    // Send a trace activity, which will be displayed in Bot Framework Emulator
    await context.sendTraceActivity(
        'OnTurnError Trace',
        `${ error }`,
        'https://www.botframework.com/schemas/error',
        'TurnError'
    );

    // Send a message to the user
    await context.sendActivity('The bot encountered an error or bug.');
    await context.sendActivity('To continue to run this bot, please fix the bot source code.');
};

// Set the onTurnError for the singleton BotFrameworkAdapter.
adapter.onTurnError = onTurnErrorHandler;

// Create the main dialog.
const myBot = new EchoBot();

// Listen for incoming requests.
server.post('/api/messages', (req, res) => {
    adapter.processActivity(req, res, async (context) => {
        // Route to main dialog.
        await myBot.run(context);
    });
});

// Listen for Upgrade requests for Streaming.
server.on('upgrade', (req, socket, head) => {
    // Create an adapter scoped to this WebSocket connection to allow storing session data.
    const streamingAdapter = new BotFrameworkAdapter({
        appId: process.env.MicrosoftAppId,
        appPassword: process.env.MicrosoftAppPassword
    });
    // Set onTurnError for the BotFrameworkAdapter created for each connection.
    streamingAdapter.onTurnError = onTurnErrorHandler;

    streamingAdapter.useWebSocket(req, socket, head, async (context) => {
        // After connecting via WebSocket, run this logic for every request sent over
        // the WebSocket connection.
        await myBot.run(context);
    });
});

const manifest = {
    // "$schema": "https://schemas.botframework.com/schemas/skills/skill-manifest-2.0.0.json",
   "$schema": "https://schemas.botframework.com/schemas/skills/v2.1/skill-manifest.json",
    "$id": "softphonebot",
    "name": "Softphone's Skill bot",
    "version": "1.0",
    "publisherName": "Softphone",
    "description": "This is a sample skill bot for Softphone",
//    "privacyUrl": "https://echoskillbot.contoso.com/privacy.html",
    "copyright": "Copyright (c) Microsoft Corporation. All rights reserved.",
    "license": "",
//    "iconUrl": "https://echoskillbot.contoso.com/icon.png",
    // "tags": [
    //   "sample",
    //   "echo"
    // ],
    "endpoints": [
      {
        "name": "default",
        "protocol": "BotFrameworkV3",
        "description": "Default endpoint for the skill",
        "endpointUrl": "https://softphonebot.azurewebsites.net/api/messages",
        "msAppId": "d73fb332-6107-463d-afa9-b79e63dd8762"
      }
    ],
    "intents": {
        "Sample": "#/activities/message",
        "*": "#/activities/message"
    },
    "activities": {
        "message": {
            "type": "message",
            "description": "Receives the users utterance and echoes"
        },
        "handoff": {
            "type": "event",
            "description": "Hand off to agent",
            "name": "handoff.initiate",
        },
    },
    // "otherActivities": {
    //     "handoff": {
    //         "type": "handoff",
    //         "description": "Hands off to a PureCloud agent",
    //     },
    // },
    "definitions": { },    
  }

server.get('/manifest', (req, res, next) => {
    res.send(manifest);
    next();
});